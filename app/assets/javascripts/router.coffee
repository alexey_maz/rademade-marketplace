app.config([
  '$stateProvider', '$urlRouterProvider', '$locationProvider',
  ($stateProvider, $urlRouterProvider, $locationProvider) ->

    $stateProvider
    .state 'public',
      abstract: true,
      url: '',
      templateUrl: 'layouts/index.html',
      controller: 'layout'

    .state 'public.supplies',
      url: '/supplies?page=',
      templateUrl: 'views/index.html'
      controller: 'SupplyListCtrl'

    .state 'public.details',
      url: '/supplies/:supplySlug',
      templateUrl: 'views/details.html',
      controller: 'SupplyDetailCtrl'

    $urlRouterProvider.otherwise '/supplies'

    $locationProvider.html5Mode
      enabled: true
      requireBase: false
      html5Mode: true

])
