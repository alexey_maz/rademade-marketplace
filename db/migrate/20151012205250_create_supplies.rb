class CreateSupplies < ActiveRecord::Migration
  def change
    create_table :supplies do |t|
      t.string :name
      t.text :description
      t.decimal :price

      t.timestamps
    end
  end
end
