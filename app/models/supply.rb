class Supply < ActiveRecord::Base
  before_create :add_slug

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :description, presence: true
  validates :price, presence: true

private
  def add_slug
    self.slug = name.split(/\W+/).join("-").downcase
  end
end
