app.controller 'SupplyListCtrl', [
  '$scope',
  '$stateParams',
  '$state', 
  'Supply',
  ($scope, $stateParams, $state, Supply) ->
    activate = () ->
      $scope.currentPage = $stateParams.page
      getSupplies($scope.currentPage)

    getSupplies = (page) ->
      Supply.get('', {"page": page})
      .then (supplies)->
        $scope.supplies = supplies.items
        $scope.totalItems = supplies.count
        $scope.totalPages = supplies.count / 5

    $scope.pageChanged = () ->
      $state.go('public.supplies', { page: $scope.currentPage }, { notify: false })
      getSupplies($scope.currentPage)

    activate()

]
