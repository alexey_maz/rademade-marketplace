class Api::SuppliesController < Api::ApplicationController
  respond_to :json

  def index
    current_page = index_params[:page]
    @supplies = Supply.page(current_page)

    respond_with(count: Supply.count, items: @supplies)
  end

  def show
    @supply = Supply.find_by(slug: show_params[:slug])

    if @supply.nil?
      render json: { errors: ["Record not found"] }, status: 404
    else 
      respond_with @supply
    end
  end

private

  def index_params
    params.permit(:page)
  end

  def show_params
    params.permit(:slug)
  end
end
