app.controller 'SupplyDetailCtrl', [ 
  '$scope', 
  '$stateParams', 
  'Supply', 
  ($scope, $stateParams, Supply) ->

    $scope.error = false

    Supply.get('/' + $stateParams.supplySlug)
    .then (supply)-> $scope.supply = supply
    .catch (error)-> $scope.error = true

  
]
