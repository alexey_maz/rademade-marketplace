require "rails_helper"

describe Api::SuppliesController, type: :controller do
  describe "GET #index" do
    let!(:supply_1) { create(:supply) }
    let!(:supply_2) { create(:supply) }

    context 'without pagination' do
      it 'responds with status 200' do
        get :index, format: :json

        expect(response).to be_success
      end

      it 'returns correct json' do
        get :index, format: :json

        result = JSON.parse(response.body)
        expect(result['count']).to eq(2)
        expect(result['items'].map{|i| i['id'] }).to eq([
          supply_1.id, 
          supply_2.id
        ])
      end
    end

    context 'with pagination' do
      before { Kaminari.configure { |config| config.default_per_page = 1 } }
      
      it 'should return appropriate page' do
        get :index, page: 2, format: :json
        
        result = JSON.parse(response.body)
        expect(result['items'].map {|i| i['id']}).to eq([
          supply_2.id
        ])
      end
      
      after { Kaminari.configure { |config| config.default_per_page = 5 } }
    end
  end

  describe "GET #show" do
    let!(:supply_1) { create(:supply) }

    context 'with invalid slug' do
      it 'responds with status 404' do
        get :show, slug: "a"*100

        expect(response).to have_http_status(404)
      end
    end

    context 'with valid slug' do
      it 'responds with status 200' do
        get :show, slug: supply_1.slug, format: :json

        expect(response).to have_http_status(200)
      end

      it 'returns correct item' do
        get :show, slug: supply_1.slug, format: :json

        result = JSON.parse(response.body)
        expect(result["slug"]).to eql supply_1.slug
      end
    end
  end
end
