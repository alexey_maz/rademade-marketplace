# MarketPlace Task (fork of rademade skeleton app)
-----

## Set up project
```
bundle install
rake bower:install
rake rademade_admin:bower:install
```
## Set up database
```
rake db:create
rake db:migrate
```
## Seed database
`rake db:seed`
## Run Project
`rails s`

## Test
`bundle exec rspec`

-----

# Demo

[You can view demo here](https://rademade-marketplace.herokuapp.com/supplies)

-----

# API docs

##Index supplies##

**URL**

`/api/supplies`

**Method:**

`GET`
  
**URL Params**

 *Optional:*

 `page=[integer]`

**Success Response:**
```
Code: 200
Content: 
{"count":1,
  "items":[{
  "id":1,
  "name":"iPhone 6s Rose Gold",
  "description":"Good color, best architecture",
  "price":"700.0",
  "created_at":"2015-10-14T09:12:18.450Z",
  "updated_at":"2015-10-14T09:12:18.450Z",
  "slug":"iphone-6s-rose-gold"}]}
```

##Show supply##

**URL**

`/api/supplies/:slug`

**Method:**

`GET`

**Success Response:**

```
Code: 200
Content: 
{"id":1,
"name":"iPhone 6s Rose Gold",
"description":"Good color, best architecture",
"price":"700.0",
"created_at":"2015-10-14T09:12:18.450Z",
"updated_at":"2015-10-14T09:12:18.450Z",
"slug":"iphone-6s-rose-gold"}
```

**Error Response:**

```
Code: 404
Content: 
{"errors":["Record not found"]}
```