app.factory 'Supply', ['AppModel', (AppModel) ->

  class Supply extends AppModel
    @configure url: '/supplies', name: 'supplies', page: 2
]
