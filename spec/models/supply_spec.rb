describe Supply do
  context 'validations' do
    before { FactoryGirl.build(:supply) }

    it do
      is_expected.to validate_uniqueness_of(:name).
        case_insensitive
    end

    it do
      is_expected.to validate_presence_of(:description)
    end

    it do
      is_expected.to validate_presence_of(:price)
    end
  end

  context 'slug generation' do
    let!(:supply_1) { FactoryGirl.create(:supply, name: 'My Favorite Supply') }

    it 'should be correct' do
      expect(supply_1.slug).to eql('my-favorite-supply')
    end
  end
end
