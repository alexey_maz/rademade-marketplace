class AddSlugField < ActiveRecord::Migration
  def change
    add_column :supplies, :slug, :string
  end
end
