FactoryGirl.define do 
  factory :supply do
    sequence(:name) { |n| "name-#{n}" }
    sequence(:description) { |n| "Best product for last #{n} years." }
    sequence(:price) { |n| n % 100 }
  end
end
